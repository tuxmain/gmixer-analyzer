use crate::common::*;

use std::collections::{hash_map::Entry as HashMapEntry, HashMap};

/// Assumes txs is ordered by block_id
pub fn retro_tx(
	clients_index: &HashMap<Pubkey, Client>,
	layers: usize,
	nodes_index: &HashMap<Pubkey, Node>,
	settings: &Settings,
	tx: &Tx,
	txs: &[Tx],
) -> PathResult {
	let from_block = BlockId(tx.block_id.0.saturating_sub(settings.expire_local));
	if layers == 1 {
		let mut result: Vec<MixId> = vec![];
		for txi in txs {
			if txi.block_id >= tx.block_id {
				break;
			}
			if txi.block_id >= from_block
				&& txi.receiver == tx.sender
				&& txi.tx_type == TxType::Input
				&& txi.amount == tx.amount
			{
				result.push(txi.mix_id);
			}
		}
		PathResult::Input(result)
	} else {
		let mut result: Vec<PathResult> = vec![];
		for txi in txs {
			if txi.block_id >= tx.block_id {
				break;
			}
			if txi.block_id > from_block
				&& txi.receiver == tx.sender
				&& txi.tx_type == TxType::Internal
				&& txi.amount == tx.amount
			{
				result.push(retro_tx(
					clients_index,
					layers - 1,
					nodes_index,
					settings,
					txi,
					txs,
				));
			}
		}
		PathResult::Internal(result)
	}
}

pub fn count_possible_inputs(mix_ids: &mut HashMap<MixId, usize>, path: PathResult) {
	match path {
		PathResult::Input(v) => {
			for mix_id in v {
				match mix_ids.entry(mix_id) {
					HashMapEntry::Occupied(e) => {
						let e = *e.get();
						mix_ids.insert(mix_id, e + 1);
					}
					HashMapEntry::Vacant(e) => {
						e.insert(1);
					}
				}
			}
		}
		PathResult::Internal(v) => {
			for subpath in v {
				count_possible_inputs(mix_ids, subpath);
			}
		}
	}
}
