use crate::common::*;
use crate::utils::{forensics, statistics};

use indicatif::{ProgressBar, ProgressStyle};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use std::collections::HashMap;

fn analyze_tx(
	clients_index: &HashMap<Pubkey, Client>,
	nodes_index: &HashMap<Pubkey, Node>,
	settings: &Settings,
	tx: &Tx,
	txs: &[Tx],
) -> TxResult {
	if let TxType::Output(input_block) = tx.tx_type {
		let path = forensics::retro_tx(
			clients_index,
			settings.layers,
			nodes_index,
			settings,
			tx,
			txs,
		);

		let mut mix_ids = HashMap::new();
		forensics::count_possible_inputs(&mut mix_ids, path);
		let (sum, min, max, std_dev) = statistics::compute_stats(&mix_ids);

		TxResult {
			duration: tx.block_id.0 - input_block.0,
			mean: Proba(1.0 / mix_ids.len() as f64),
			std_dev: Proba(std_dev as f64 / sum as f64),
			max: Proba(max as f64 / sum as f64),
			min: Proba(min as f64 / sum as f64),
			true_proba: Proba(*mix_ids.get(&tx.mix_id).unwrap() as f64 / sum as f64),
		}
	} else {
		panic!("Not an output tx");
	}
}

pub fn analyze(
	clients_index: &HashMap<Pubkey, Client>,
	nodes_index: &HashMap<Pubkey, Node>,
	settings: &Settings,
	txs: &[Tx],
) -> Vec<(BlockId, TxResult)> {
	let progress_bar = ProgressBar::new(txs.len() as u64);
	progress_bar.set_style(
		ProgressStyle::default_bar()
			.template("{elapsed_precise}  {eta}  {per_sec}  {pos}/{len}  {percent}% {wide_bar}"),
	);
	txs.par_iter()
		.filter_map(|tx| {
			progress_bar.inc(1);
			match tx.tx_type {
				TxType::Output(_) => Some((
					tx.block_id,
					analyze_tx(clients_index, nodes_index, settings, tx, txs),
				)),
				_ => None,
			}
		})
		.collect()
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_analyze_tx() {
		let clients: Vec<Client> = (0..5).map(|i| Client { pubkey: Pubkey(i) }).collect();
		let nodes: Vec<Node> = (5..10)
			.map(|i| Node {
				pubkey: Pubkey(i),
				pending_txs: HashMap::new(),
			})
			.collect();
		let mut clients_index: HashMap<Pubkey, Client> = HashMap::new();
		clients.into_iter().for_each(|client| {
			clients_index.insert(client.pubkey.clone(), client);
		});
		let mut nodes_index: HashMap<Pubkey, Node> = HashMap::new();
		nodes.into_iter().for_each(|node| {
			nodes_index.insert(node.pubkey.clone(), node);
		});
		let settings = Settings::default();
		let txs = vec![
			Tx {
				amount: Amount(100),
				block_id: BlockId(0),
				mix_id: MixId(0),
				receiver: Pubkey(5),
				sender: Pubkey(0),
				tx_type: TxType::Input,
			},
			Tx {
				amount: Amount(100),
				block_id: BlockId(1),
				mix_id: MixId(1),
				receiver: Pubkey(8),
				sender: Pubkey(1),
				tx_type: TxType::Input,
			},
			Tx {
				amount: Amount(100),
				block_id: BlockId(3),
				mix_id: MixId(0),
				receiver: Pubkey(6),
				sender: Pubkey(5),
				tx_type: TxType::Internal,
			},
			Tx {
				amount: Amount(100),
				block_id: BlockId(4),
				mix_id: MixId(1),
				receiver: Pubkey(6),
				sender: Pubkey(8),
				tx_type: TxType::Internal,
			},
			Tx {
				amount: Amount(100),
				block_id: BlockId(8),
				mix_id: MixId(0),
				receiver: Pubkey(7),
				sender: Pubkey(6),
				tx_type: TxType::Internal,
			},
			Tx {
				amount: Amount(100),
				block_id: BlockId(8),
				mix_id: MixId(1),
				receiver: Pubkey(9),
				sender: Pubkey(6),
				tx_type: TxType::Internal,
			},
			Tx {
				amount: Amount(100),
				block_id: BlockId(12),
				mix_id: MixId(0),
				receiver: Pubkey(1),
				sender: Pubkey(7),
				tx_type: TxType::Output(BlockId(0)),
			},
			Tx {
				amount: Amount(100),
				block_id: BlockId(15),
				mix_id: MixId(1),
				receiver: Pubkey(2),
				sender: Pubkey(9),
				tx_type: TxType::Output(BlockId(1)),
			},
		];
		assert_eq!(
			analyze_tx(
				&clients_index,
				&nodes_index,
				&settings,
				&txs.get(6).unwrap(),
				&txs
			),
			TxResult {
				duration: 12,
				max: Proba(0.5),
				mean: Proba(0.5),
				min: Proba(0.5),
				std_dev: Proba(0.0),
				true_proba: Proba(0.5),
			}
		);
		assert_eq!(
			analyze_tx(
				&clients_index,
				&nodes_index,
				&settings,
				&txs.get(7).unwrap(),
				&txs
			),
			TxResult {
				duration: 14,
				max: Proba(0.5),
				mean: Proba(0.5),
				min: Proba(0.5),
				std_dev: Proba(0.0),
				true_proba: Proba(0.5),
			}
		);
	}
}
