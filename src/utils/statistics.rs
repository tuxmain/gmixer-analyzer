use crate::common::*;

use std::collections::HashMap;

/// Returns (sum, min, max, std_dev)
pub fn compute_stats(v: &HashMap<MixId, usize>) -> (usize, usize, usize, f64) {
	let mut sum2: usize = 0; // sum of the squares
	let mut sum: usize = 0; // sum
	let mut min = usize::max_value();
	let mut max = usize::min_value();
	v.iter().for_each(|(_, x)| {
		sum2 += x.pow(2);
		sum += x;
		if *x < min {
			min = *x;
		}
		if *x > max {
			max = *x;
		}
	});
	(
		sum,
		min,
		max,
		(sum2 as f64 / v.len() as f64 - (sum as f64 / v.len() as f64).powi(2)).sqrt(),
	)
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_stats() {
		let mut mix_ids: HashMap<MixId, usize> = HashMap::new();
		mix_ids.insert(MixId(0), 1);
		mix_ids.insert(MixId(1), 2);
		mix_ids.insert(MixId(2), 3);
		mix_ids.insert(MixId(3), 4);
		mix_ids.insert(MixId(4), 5);

		let (sum, min, max, std_dev) = compute_stats(&mix_ids);

		assert_eq!(sum, 15);
		assert_eq!(min, 1);
		assert_eq!(max, 5);
		assert!((std_dev - 1.4142135623730951).abs() < 0.00001);
	}
}
