use crate::common::*;

use rand::Rng;
use std::collections::{hash_map::Entry as HashMapEntry, HashMap};

pub fn prepare(
	clients_index: &mut HashMap<Pubkey, Client>,
	nodes_index: &mut HashMap<Pubkey, Node>,
	settings: &Settings,
) {
	for i in 0..settings.nb_clients {
		clients_index.insert(Pubkey(i), Client { pubkey: Pubkey(i) });
	}
	for i in settings.nb_clients..(settings.nb_clients + settings.nb_nodes) {
		nodes_index.insert(
			Pubkey(i),
			Node {
				pubkey: Pubkey(i),
				pending_txs: HashMap::new(),
			},
		);
	}
}

/// Returns: expired mixes
pub fn simulate(
	clients_index: &HashMap<Pubkey, Client>,
	nodes_index: &mut HashMap<Pubkey, Node>,
	settings: &Settings,
	txs: &mut Vec<Tx>,
) -> Vec<usize> {
	let mut rng = rand::thread_rng();

	let clients_pubkeys: Vec<Pubkey> = clients_index.iter().map(|x| x.1.pubkey).collect();
	let mut nodes_pubkeys: Vec<Pubkey> = nodes_index.iter().map(|x| x.1.pubkey).collect();
	let mut mix_id = MixId(0);
	let mut expired: Vec<usize> = Vec::new();

	let mut amounts_iter = settings.amounts.iter();
	let mut current_amounts: &ProbaList<Amount>;
	let mut next_amount: &BlockId;
	{
		let first_amounts = amounts_iter
			.next()
			.expect("There must be at least 1 list of amounts!");
		next_amount = &first_amounts.next;
		current_amounts = &first_amounts.val;
	}

	for block_id in 0..settings.nb_blocks {
		let mut current_expired: usize = 0;

		if block_id == next_amount.0 {
			if let Some(IndexedProbaList { next: a, val: b }) = amounts_iter.next() {
				next_amount = a;
				current_amounts = b;
			}
		}

		let mut new_pending_txs: HashMap<Pubkey, Vec<PendingTx>> = HashMap::new();
		for node in nodes_index.values_mut() {
			for pending_txs in node.pending_txs.values_mut() {
				let mut to_remove: Vec<usize> = vec![];
				for (i, pending_tx) in pending_txs.iter().enumerate() {
					if pending_tx.expire_local.0 <= block_id
						|| pending_tx.expire_global.0 <= block_id
					{
						to_remove.push(i);
					}
				}
				current_expired += to_remove.len();
				for i in to_remove.iter().rev() {
					pending_txs.remove(*i);
				}
			}

			let mut to_send: Vec<Amount> = vec![];
			for (amount, pending_txs) in node.pending_txs.iter() {
				if pending_txs.len() >= settings.txmin {
					to_send.push(*amount);
				}
			}
			for amount in to_send {
				for pending_tx in node.pending_txs.remove(&amount).unwrap() {
					if pending_tx.path.len() > 1 {
						let new_pending_tx = PendingTx {
							amount,
							expire_global: pending_tx.expire_global,
							expire_local: BlockId(block_id + settings.expire_local),
							input_block: pending_tx.input_block,
							mix_id: pending_tx.mix_id,
							path: pending_tx.path[1..].to_vec(),
						};
						match new_pending_txs.entry(pending_tx.path[0]) {
							HashMapEntry::Occupied(mut e) => e.get_mut().push(new_pending_tx),
							HashMapEntry::Vacant(e) => {
								e.insert(vec![new_pending_tx]);
							}
						}
					}
					txs.push(Tx {
						amount,
						block_id: BlockId(block_id),
						mix_id: pending_tx.mix_id,
						receiver: pending_tx.path[0],
						sender: node.pubkey,
						tx_type: if pending_tx.path.len() > 1 {
							TxType::Internal
						} else {
							TxType::Output(pending_tx.input_block)
						},
					});
				}
			}
		}
		for (pubkey, pending_txs) in new_pending_txs {
			let node = nodes_index.get_mut(&pubkey).unwrap();
			for pending_tx in pending_txs {
				match node.pending_txs.entry(pending_tx.amount) {
					HashMapEntry::Occupied(mut e) => e.get_mut().push(pending_tx),
					HashMapEntry::Vacant(e) => {
						e.insert(vec![pending_tx]);
					}
				}
			}
		}
		expired.push(current_expired);

		for client in clients_index.values() {
			if rng.gen_bool(settings.txs_per_block_per_client.0) {
				let amount = *current_amounts.choose().unwrap();

				let mut path: Vec<Pubkey> = Vec::new();
				// Fisher-Yates shuffle
				if settings.smart_clients {
					// Smart client: pick in priority nodes with pending_txs < txmin
					let mut path2: Vec<Pubkey> = Vec::new();
					for (i, (pubkey, node)) in nodes_index.iter().enumerate() {
						let r: usize = rand::Rng::gen_range(&mut rng, i..nodes_pubkeys.len());
						if let Some(pending_txs) = node.pending_txs.get(&amount) {
							if pending_txs.len() < settings.txmin {
								path.push(*pubkey);
								if path.len() == settings.layers {
									break;
								}
							} else if path2.len() < settings.layers {
								path2.push(*pubkey);
							}
						} else {
							path.push(*pubkey);
							if path.len() == settings.layers {
								break;
							}
						}
						nodes_pubkeys.swap(i, r);
					}
					while path.len() < settings.layers {
						path.push(path2.pop().expect("Not enough nodes"));
					}
				} else {
					// Simple client: equiprobably pick nodes
					for layer in 0..settings.layers {
						let r: usize = rand::Rng::gen_range(&mut rng, layer..nodes_pubkeys.len());
						path.push(nodes_pubkeys[r]);
						nodes_pubkeys.swap(layer, r);
					}
				}
				path.push(clients_pubkeys[rand::Rng::gen_range(&mut rng, 0..clients_index.len())]);
				let tx = Tx {
					block_id: BlockId(block_id),
					amount,
					mix_id,
					sender: client.pubkey,
					receiver: path[0],
					tx_type: TxType::Input,
				};
				if settings.layers > 0 {
					let mut next_path = path.clone();
					next_path.remove(0);
					let node = nodes_index.get_mut(&path[0]).unwrap();
					let pending_tx = PendingTx {
						amount: tx.amount,
						expire_global: BlockId(block_id + settings.expire_global),
						expire_local: BlockId(block_id + settings.expire_local),
						input_block: BlockId(block_id),
						mix_id,
						path: next_path,
					};
					match node.pending_txs.entry(tx.amount) {
						HashMapEntry::Occupied(mut e) => e.get_mut().push(pending_tx),
						HashMapEntry::Vacant(e) => {
							e.insert(vec![pending_tx]);
						}
					}
				}
				mix_id.0 += 1;
				txs.push(tx);
			}
		}
	}
	expired
}
