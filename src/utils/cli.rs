use crate::common::*;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct ChartsSubcommand {
	#[structopt(short = "i")]
	pub input_format: ExportFormat,
}

#[derive(Debug, StructOpt)]
pub struct ClientSubcommand {}

#[derive(Debug, StructOpt)]
pub struct ConvertSubcommand {
	#[structopt(short = "i")]
	pub input_format: ExportFormat,

	#[structopt(short = "o")]
	pub output_format: ExportFormat,
}

#[derive(Debug, StructOpt)]
pub struct LocalSubcommand {
	/// Export format
	///
	/// Format in which results written in stdout are encoded
	/// Available formats: bincode, json
	#[structopt(
		default_value = "bincode",
		long = "export",
		name = "format",
		short = "e"
	)]
	pub export: ExportFormat,

	#[structopt(flatten)]
	pub settings: Settings,
}

#[derive(Debug, StructOpt)]
pub struct ServerSubcommand {}

#[derive(Debug, StructOpt)]
pub enum MainSubcommand {
	Charts(ChartsSubcommand),
	Client(ClientSubcommand),
	Convert(ConvertSubcommand),
	Local(LocalSubcommand),
	Server(ServerSubcommand),
}

#[derive(Debug, StructOpt)]
#[structopt(
	name = "gmixer_analyzer",
	about = "ĞMixer simulator and forensics tool, for evaluating security of given parameters"
)]
pub struct MainOpt {
	#[structopt(subcommand)]
	pub cmd: MainSubcommand,

	/// Number of threads [default: number of CPUs]
	#[structopt(long = "threads", short = "T")]
	pub threads: Option<usize>,
}
