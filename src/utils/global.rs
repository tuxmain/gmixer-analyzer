use crate::common::*;
use crate::utils::{analyze, simulation};

use std::collections::HashMap;

pub fn run(settings: &Settings) -> (Vec<usize>, Vec<(BlockId, TxResult)>) {
	let mut clients_index: HashMap<Pubkey, Client> = HashMap::new();
	let mut nodes_index: HashMap<Pubkey, Node> = HashMap::new();
	let mut txs = Vec::<Tx>::new();

	simulation::prepare(&mut clients_index, &mut nodes_index, settings);

	let expired = simulation::simulate(&clients_index, &mut nodes_index, settings, &mut txs);
	eprintln!("Simulated {} txs", txs.len());

	let results = analyze::analyze(&clients_index, &nodes_index, settings, &txs);
	eprintln!("Analyzed {} mixes", results.len());

	(expired, results)
}
