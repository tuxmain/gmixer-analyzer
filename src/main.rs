#![feature(option_result_unwrap_unchecked)]

mod charts;
mod client;
mod common;
mod convert;
mod local;
mod server;
mod utils;

use utils::cli;

use structopt::StructOpt;

fn main() {
	let opt = cli::MainOpt::from_args();

	if let Some(num_threads) = opt.threads {
		rayon::ThreadPoolBuilder::new()
			.num_threads(num_threads)
			.build_global()
			.unwrap();
	}

	match opt.cmd {
		cli::MainSubcommand::Client(cmd) => client::run_client(cmd),
		cli::MainSubcommand::Convert(cmd) => convert::run_convert(cmd),
		cli::MainSubcommand::Charts(cmd) => charts::run_charts(cmd),
		cli::MainSubcommand::Local(cmd) => local::run_local(cmd),
		cli::MainSubcommand::Server(cmd) => server::run_server(cmd),
	}
}
