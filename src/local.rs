use crate::common::*;
use crate::utils::*;

use std::{io, io::Write};

pub fn run_local(opt: cli::LocalSubcommand) {
	let (expired, results) = global::run(&opt.settings);

	let export_results = ExportResults {
		expired,
		results,
		settings: opt.settings,
		version: VERSION,
	};

	let stdout = io::stdout();
	let mut handle = stdout.lock();
	handle
		.write_all(&match opt.export {
			ExportFormat::Bincode => bincode::serialize(&export_results).unwrap(),
			ExportFormat::Cbor => serde_cbor::to_vec(&export_results).unwrap(),
			ExportFormat::Json => serde_json::to_string(&export_results).unwrap().into_bytes(),
		})
		.unwrap();
}
