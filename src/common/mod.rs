use serde::{Deserialize, Serialize};
use std::{
	cmp::Ordering, collections::HashMap, convert::From, io, num::ParseFloatError, str::FromStr,
};
use structopt::StructOpt;

pub const VERSION: u32 = 0;

#[derive(PartialEq)]
pub struct NonNan<T>(pub T);

impl<T> Eq for NonNan<T> where T: PartialEq {}

impl<T> PartialOrd for NonNan<T>
where
	T: PartialOrd,
{
	fn partial_cmp(&self, other: &NonNan<T>) -> Option<Ordering> {
		self.0.partial_cmp(&other.0)
	}
}

impl<T> Ord for NonNan<T>
where
	T: PartialOrd,
{
	fn cmp(&self, other: &NonNan<T>) -> Ordering {
		unsafe { self.partial_cmp(other).unwrap_unchecked() }
	}
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Pubkey(pub u32);

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub struct BlockId(pub u32);

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct Amount(pub u32);

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct MixId(pub u32);

#[derive(Debug, PartialEq)]
pub enum TxType {
	Input,
	Internal,
	/// Contains input tx block_id
	Output(BlockId),
}

#[derive(Debug)]
pub struct Tx {
	pub amount: Amount,
	pub block_id: BlockId,
	pub mix_id: MixId,
	pub receiver: Pubkey,
	pub sender: Pubkey,
	pub tx_type: TxType,
}

#[derive(Debug)]
pub struct PendingTx {
	pub amount: Amount,
	pub expire_local: BlockId,
	pub expire_global: BlockId,
	pub input_block: BlockId,
	pub mix_id: MixId,
	pub path: Vec<Pubkey>,
}

#[derive(Debug)]
pub struct Node {
	pub pending_txs: HashMap<Amount, Vec<PendingTx>>,
	pub pubkey: Pubkey,
}

#[derive(Debug)]
pub struct Client {
	pub pubkey: Pubkey,
}

#[derive(Clone, Debug, Deserialize, PartialEq, PartialOrd, Serialize)]
pub struct Proba(pub f64);

impl From<f64> for Proba {
	#[inline]
	fn from(v: f64) -> Self {
		Self(v)
	}
}

impl FromStr for Proba {
	type Err = ParseFloatError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(Self::from(s.parse::<f64>()?))
	}
}

#[derive(Debug, Deserialize, Serialize)]
struct ProbaEvent<T> {
	pub event: T,
	pub proba: Proba,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ProbaList<T>(Vec<ProbaEvent<T>>);

impl<T> ProbaList<T> {
	/// Choose randomly an event respecting probabilities
	/// Assumes that the list is ordered by increasing proba
	pub fn choose(&self) -> Option<&T> {
		let n: f64 = rand::random();
		let mut s: f64 = 0.0;
		for proba_event in &self.0 {
			s += proba_event.proba.0;
			if n < s {
				return Some(&proba_event.event);
			}
		}
		None
	}
}

#[derive(Debug, Deserialize, Serialize)]
pub struct IndexedProbaList<I, T> {
	pub next: I,
	pub val: ProbaList<T>,
}

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct TxResult {
	pub duration: u32,
	pub mean: Proba,
	pub std_dev: Proba,
	pub max: Proba,
	pub min: Proba,
	pub true_proba: Proba,
}

#[derive(Debug)]
pub enum PathResult {
	Input(Vec<MixId>),
	Internal(Vec<PathResult>),
}

#[derive(Debug, Deserialize, Serialize, StructOpt)]
pub struct Settings {
	/// List of possible amounts with probas
	#[structopt(default_value="{\"next\":0,\"val\":[{\"event\":100,\"proba\":1.0}]}", long="amounts", parse(try_from_str=serde_json::from_str), short="a")]
	pub amounts: Vec<IndexedProbaList<BlockId, Amount>>,

	/// Expiration timeout for each pending tx (blocks)
	#[structopt(default_value = "288", long = "elocal", short = "L")]
	pub expire_local: u32,

	/// Expiration timeout for each mix process (blocks)
	#[structopt(default_value = "1008", long = "eglobal", short = "G")]
	pub expire_global: u32,

	/// Number of onion layers
	#[structopt(default_value = "3", long = "layers", short = "l")]
	pub layers: usize,

	/// Number of blocks
	#[structopt(default_value = "4032", long = "blocks", short = "t")]
	pub nb_blocks: u32,

	/// Number of clients
	#[structopt(default_value = "200", long = "clients", short = "c")]
	pub nb_clients: u32,

	/// Number of nodes
	#[structopt(default_value = "20", long = "nodes", short = "n")]
	pub nb_nodes: u32,

	/// Enable smart clients
	#[structopt(
		default_value = "true",
		long = "smart",
		parse(try_from_str),
		short = "s"
	)]
	pub smart_clients: bool,

	/// Minimal number of pending txs to mix
	#[structopt(default_value = "5", long = "txmin", short = "x")]
	pub txmin: usize,

	/// Avg txs per block per client
	#[structopt(default_value = "0.0035", long = "txsbc", short = "p")]
	pub txs_per_block_per_client: Proba,
}

impl Default for Settings {
	fn default() -> Self {
		Self {
			amounts: vec![IndexedProbaList {
				next: BlockId(0),
				val: ProbaList(vec![ProbaEvent {
					event: Amount(100),
					proba: Proba(1.0),
				}]),
			}],
			expire_local: 288,
			expire_global: 1008,
			layers: 3,
			nb_blocks: 4032,
			nb_clients: 200,
			nb_nodes: 20,
			smart_clients: true,
			txmin: 5,
			txs_per_block_per_client: Proba(0.0035),
		}
	}
}

#[derive(Debug, StructOpt)]
pub enum ExportFormat {
	Bincode,
	Cbor,
	Json,
}

impl FromStr for ExportFormat {
	type Err = io::Error;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(match s {
			"bincode" => Self::Bincode,
			"cbor" => Self::Cbor,
			"json" => Self::Json,
			_ => {
				return Err(Self::Err::new(
					io::ErrorKind::Other,
					"Unknown export format",
				));
			}
		})
	}
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ExportResults {
	pub expired: Vec<usize>,
	pub results: Vec<(BlockId, TxResult)>,
	pub settings: Settings,
	pub version: u32,
}
