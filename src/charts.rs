use crate::common::*;
use crate::utils::cli;

use plotters::prelude::*;
use std::{
	collections::{hash_map::Entry as HashMapEntry, HashMap},
	io::Read,
};

pub fn run_charts(opt: cli::ChartsSubcommand) {
	let stdin = std::io::stdin();
	let mut handle = stdin.lock();
	let mut raw = Vec::new();
	handle.read_to_end(&mut raw).unwrap();
	let export_results: ExportResults = match opt.input_format {
		ExportFormat::Bincode => bincode::deserialize(&raw).unwrap(),
		ExportFormat::Cbor => serde_cbor::from_slice(&raw).unwrap(),
		ExportFormat::Json => serde_json::from_str(std::str::from_utf8(&raw).unwrap()).unwrap(),
	};

	let root = BitMapBackend::new("security.png", (1280, 720)).into_drawing_area();
	root.fill(&WHITE).unwrap();

	let mean_max: f32 = export_results
		.results
		.iter()
		.map(|(_, tx_result)| NonNan(tx_result.mean.0 as f32))
		.max()
		.unwrap()
		.0;
	let mean_min: f32 = export_results
		.results
		.iter()
		.map(|(_, tx_result)| NonNan(tx_result.mean.0 as f32))
		.min()
		.unwrap()
		.0;
	let std_dev_max: f32 = export_results
		.results
		.iter()
		.map(|(_, tx_result)| NonNan(tx_result.std_dev.0 as f32))
		.max()
		.unwrap()
		.0;

	let mut chart = ChartBuilder::on(&root)
		.margin(4)
		.caption("ĞMixer security", ("sans-serif", 30.0).into_font())
		.x_label_area_size(30)
		.y_label_area_size(40)
		.right_y_label_area_size(40)
		.build_cartesian_2d(0u32..export_results.settings.nb_blocks, mean_min..mean_max)
		.unwrap()
		.set_secondary_coord(0u32..export_results.settings.nb_blocks, 0f32..std_dev_max);

	chart
		.configure_mesh()
		.x_desc("Blocks")
		.y_desc("Probability")
		.draw()
		.unwrap();

	chart
		.configure_secondary_axes()
		.y_desc("Probability standard deviation")
		.draw()
		.unwrap();

	chart
		.draw_series(PointSeries::of_element(
			export_results
				.results
				.iter()
				.map(|(block_id, tx_result)| (block_id.0, tx_result.mean.0 as f32))
				.collect::<Vec<(u32, f32)>>(),
			1,
			&RED,
			&|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
		))
		.unwrap()
		.label("Mean (left)")
		.legend(move |(x, y)| Rectangle::new([(x - 5, y - 5), (x + 5, y + 5)], &RED));

	chart
		.draw_secondary_series(PointSeries::of_element(
			export_results
				.results
				.iter()
				.map(|(block_id, tx_result)| (block_id.0, tx_result.std_dev.0 as f32))
				.collect::<Vec<(u32, f32)>>(),
			1,
			&BLUE,
			&|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
		))
		.unwrap()
		.label("Std dev (right)")
		.legend(move |(x, y)| Rectangle::new([(x - 5, y - 5), (x + 5, y + 5)], &BLUE));

	chart
		.configure_series_labels()
		.background_style(&WHITE.mix(0.8))
		.border_style(&BLACK)
		.draw()
		.unwrap();

	let root = BitMapBackend::new("effectiveness.png", (1280, 720)).into_drawing_area();
	root.fill(&WHITE).unwrap();

	let duration_max: u32 = export_results
		.results
		.iter()
		.map(|(_, tx_result)| tx_result.duration)
		.max()
		.unwrap();
	let duration_min: u32 = export_results
		.results
		.iter()
		.map(|(_, tx_result)| tx_result.duration)
		.min()
		.unwrap();
	//let expired_max: u32 = *export_results.expired.iter().max().unwrap() as u32;

	let mut tx_classes: HashMap<u32, Vec<&TxResult>> = HashMap::new();
	export_results
		.results
		.iter()
		.for_each(|(block_id, tx_result)| {
			let class_id = block_id.0.checked_sub(block_id.0 % 288).unwrap();
			match tx_classes.entry(class_id) {
				HashMapEntry::Occupied(mut e) => e.get_mut().push(tx_result),
				HashMapEntry::Vacant(e) => {
					e.insert(vec![tx_result]);
				}
			}
		});

	let expired_classes = export_results.expired.chunks(288);
	let expired_classes_sum = expired_classes
		.clone()
		.map(|x| x.iter().sum::<usize>() as u32);
	let expired_classes_max = expired_classes_sum.max().unwrap();

	let mut chart = ChartBuilder::on(&root)
		.margin(4)
		.caption("ĞMixer effectiveness", ("sans-serif", 30.0).into_font())
		.x_label_area_size(30)
		.y_label_area_size(40)
		.right_y_label_area_size(40)
		.build_cartesian_2d(
			0u32..export_results.settings.nb_blocks,
			(duration_min as f32..duration_max as f32).log_scale(),
		)
		.unwrap()
		.set_secondary_coord(
			0u32..export_results.settings.nb_blocks,
			0u32..expired_classes_max,
		);

	chart
		.configure_mesh()
		.y_label_formatter(&|&x| format!("{:.0}", x))
		.x_desc("Blocks")
		.y_desc("Mix duration (blocks)")
		.draw()
		.unwrap();

	chart
		.configure_secondary_axes()
		.y_desc("Expired mixes")
		.draw()
		.unwrap();

	chart
		.draw_series(PointSeries::of_element(
			export_results
				.results
				.iter()
				.map(|(block_id, tx_result)| (block_id.0, tx_result.duration as f32))
				.collect::<Vec<(u32, f32)>>(),
			1,
			&BLUE.mix(0.25),
			&|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
		))
		.unwrap();

	chart
		.draw_series(tx_classes.iter().map(|(class_id, tx_results)| {
			Boxplot::new_vertical(
				class_id + 144,
				&Quartiles::new(
					&tx_results
						.iter()
						.map(|&x| x.duration as f32)
						.collect::<Vec<f32>>(),
				),
			)
			.style(&BLUE)
		}))
		.unwrap()
		.label("Duration (left)")
		.legend(move |(x, y)| Rectangle::new([(x - 5, y - 5), (x + 5, y + 5)], &BLUE));

	chart
		.draw_secondary_series(
			Histogram::vertical(chart.borrow_secondary())
				.style(RED.mix(0.25).filled())
				.data(
					(0u32..)
						.step_by(288)
						.map(|x| x + 144)
						.zip(expired_classes.map(|x| x.iter().sum::<usize>() as u32)),
				), //.data((0u32..).zip(export_results.expired.iter().map(|&x| x as u32))),
		)
		.unwrap()
		.label("Expired mixes (right)")
		.legend(move |(x, y)| Rectangle::new([(x - 5, y - 5), (x + 5, y + 5)], &RED));

	chart
		.configure_series_labels()
		.background_style(&WHITE.mix(0.8))
		.border_style(&BLACK)
		.draw()
		.unwrap();
}
