use crate::cli;
use crate::common::*;

use std::io::{Read, Write};

pub fn run_convert(opt: cli::ConvertSubcommand) {
	let stdin = std::io::stdin();
	let mut handle = stdin.lock();
	let mut raw = Vec::new();
	handle.read_to_end(&mut raw).unwrap();
	let export_results: ExportResults = match opt.input_format {
		ExportFormat::Bincode => bincode::deserialize(&raw).unwrap(),
		ExportFormat::Cbor => serde_cbor::from_slice(&raw).unwrap(),
		ExportFormat::Json => serde_json::from_str(std::str::from_utf8(&raw).unwrap()).unwrap(),
	};

	let stdout = std::io::stdout();
	let mut handle = stdout.lock();
	handle
		.write_all(&match opt.output_format {
			ExportFormat::Bincode => bincode::serialize(&export_results).unwrap(),
			ExportFormat::Cbor => serde_cbor::to_vec(&export_results).unwrap(),
			ExportFormat::Json => serde_json::to_string(&export_results).unwrap().into_bytes(),
		})
		.unwrap();
}
