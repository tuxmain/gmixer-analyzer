# ĞMixer analyzer

ĞMixer simulator and forensics tool, for evaluating security of given parameters.

This program generates random ĞMixer transactions with given parameters, then reverts them to measure how easily users' anonymity can be broken.

## Use

`./gmixer_analyzer --help` to display help

### Simulate & analyze

`local` subcommand takes commandline settings and writes to stdout the results in specified format (`-e` option, default `bincode`).

    # Run with default settings
    ./gmixer_analyzer local > results.bincode
    
    # Display help
    ./gmixer_analyzer local --help

### Draw charts

`charts` subcommand takes stdin as data and writes `security.png` and `effectiveness.png`.

    cat results.bincode | ./gmixer_analyzer charts -i bincode

## License

GNU AGPL v3, CopyLeft 2020 Pascal Engélibert
